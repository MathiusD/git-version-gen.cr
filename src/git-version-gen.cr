module GitVersionGen
  macro generate_version(dir)
    VERSION = {{ `shards version "#{dir}"`.chomp.stringify.downcase }}
    GIT_BRANCH = {{ "#{`git branch | sed -n '/* /s///p'`.strip}" }}
    GIT_COMMIT_SHORT = {{ "#{`git rev-list HEAD --max-count=1 --abbrev-commit`.strip}" }}
    GIT_COMMIT = {{ "#{`git rev-list HEAD --max-count=1`.strip}" }}

    FULL_VERSION = "#{VERSION}-#{GIT_BRANCH} (#{GIT_COMMIT_SHORT})"
    FULL_VERSION_LONG_COMMIT = "#{VERSION}-#{GIT_BRANCH} (#{GIT_COMMIT})"
  end

  GitVersionGen.generate_version __DIR__
end
