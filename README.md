# git-version-gen.cr

[![Pipeline Status](https://gitlab.com/MathiusD/git-version-gen.cr/badges/main/pipeline.svg)](https://gitlab.com/MathiusD/git-version-gen.cr/-/pipelines)
[![Documentation](https://img.shields.io/badge/docs-available-brightgreen.svg)](https://mathiusd.gitlab.io/git-version-gen.cr)

git-version-gen allow to generate version vars from git repository
