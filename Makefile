clean:
	@rm -rf docs
	@rm -rf reports
	@rm -rf lib
	@rm -f shard.lock

install:
	@shards install
	
format:
	@crystal tool format --check

docs:
	@crystal docs src/git-version-gen.cr